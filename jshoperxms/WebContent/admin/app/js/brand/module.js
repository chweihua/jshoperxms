define(['angular','datatables','angular-resource','ngfileupload'],function(ng){
	'use strict';
	return ng.module('brandmodule',['datatables','ngResource','ngFileUpload']);
});