define(['angular','datatables','angular-resource'],function(ng){
	'use strict';
	return ng.module('postsmodule',['datatables','ngResource']);
});