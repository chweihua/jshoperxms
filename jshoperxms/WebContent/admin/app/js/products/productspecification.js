var mModule=angular.module("productspecificationModule",[]);

/**
 * 货物规格路由控制
 */
mModule.config(function($routeProvider){
	$routeProvider
	.when('/productspecificationment',{
		templateUrl:'../admin/app/products/productspecificationment.html',
		controller:'productspecification'
	});
});

mModule.controller('productspecification',['$scope','$http',function($scope,$http){
	$scope.title="货物规格列表";
}]);

