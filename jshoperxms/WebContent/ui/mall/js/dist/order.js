$(function(){

  var tagforc=$.query.get("goodsstr");
  if(tagforc!=""&&tagforc.length>1){
    $("#isgoodsfromcart").val("1");
  }else{
    $("#isgoodsfromcart").val("0");
  }


  savedeliveraddress=function(basepath){
    var country="中国";
    var province=$("#province").val();
    if(province==""){
      $("#emsg").text("请选择地址信息");
      return false;
    }
    var city=$("#city").val();
    if(city==""){
      $("#emsg").text("请选择地址信息");
      return false;
    }
    var district=$("#area").val();
    if(district==""){
      $("#emsg").text("请选择地址信息");
      return false;
    }
    var street=$("#street").val();
    if(street==""){
      $("#emsg").text("请输入地址信息");
      return false;
    }
    if(street.length>50){
      $("#emsg").text("输入的地址太长了");
      return false;
    }
    var shippingusername=$("#shippingusername").val();
    if(shippingusername==""){
      $("#emsg").text("请输入收货人姓名");
      return false;
    }
    var mobile=$("#mobile").val();
    if(mobile==""){
      $("#emsg").text("请输入手机号码");
      return false;
    }
    var postcode=$("#postcode").val();
    var addresstag=$("#addresstag").val();
    if(addresstag==""){
      $("#emsg").text("请输入地址别称，例如家庭地址");
      return false;
    }
    $.post(basepath+"/mcenter/savedeliveraddress.action",{
      "shippingusername":shippingusername,
      "country":country,
      "province":province,
      "city":city,
      "district":district,
      "street":street,
      "postcode":postcode,
      "mobile":mobile,
      "addresstag":addresstag
    },function(data){
      if(data.sucflag){
        var url="";
        var goodsstr=$.query.get("goodsstr");
        if(goodsstr==""){
          url=basepath+"/order/initOrder.action"
        }else{
          url=basepath+"/order/initOrder.action?goodsstr="+goodsstr;
        }
        window.location.href=url;
      }else{
        //登录
        window.location.href=basepath+"/gologin.action";
      }
    });
  },

  setcurrentaddress=function(obj,id){
    $("a[name='addresslink']").each(function(){
      $(this).attr("class","");
    });
    obj.attr("class","current");
    $("#deliveraddressid").val(id);
    if(window.localStorage){
      localStorage.setItem("deliveraddressid",id);
    }
    
  },
  setcurrentlogistics=function(obj,id){
    $("a[name='logisticslink']").each(function(){
      $(this).attr("class","");
    });
    obj.attr("class","current");
    $("#logisticsid").val(id);
    if(window.localStorage){
      localStorage.setItem("logisticsid",id);
    }
  },
  setcurrentpayment=function(obj,id){
    $("a[name='paymentlink']").each(function(){
      $(this).attr("class","");
    });
    obj.attr("class","current");
    $("#paymentid").val(id);
    if(window.localStorage){
      localStorage.setItem("paymentid",id);
    }
  },
  setcurrentinvoice=function(obj,id){
    $("a[name='invoicelink']").each(function(){
      $(this).attr("class","");
    });
    obj.attr("class","current");
    $("#invoiceid").val(id);
    if(window.localStorage){
      localStorage.setItem("invoiceid",id);
    }
  },
  rebindcurrentaddress=function(){
    if(window.localStorage){
      var d=localStorage.getItem("deliveraddressid");
      $("#deliveraddressid").val(d);
    }else{
      var d=$("#deliveraddressid").val();
    }
    $("a[name='addresslink']").each(function(){
      if($(this).attr("id")=='addresslink'+d){
        $(this).attr("class","current");
      }
    });
   
  },
  rebindcurrentaddress();
  rebindcurrentlogistics=function(){
    if(window.localStorage){
      var d=localStorage.getItem("logisticsid");
      $("#logisticsid").val(d);
    }else{
      var d=$("#logisticsid").val();
    }
   
    $("a[name='logisticslink']").each(function(){
      if($(this).attr("id")=='logisticslink'+d){
        $(this).attr("class","current");
      }
    });
  },
  rebindcurrentlogistics();
  rebindcurrentpayment=function(){
    if(window.localStorage){
      var d=localStorage.getItem("paymentid");
      $("#paymentid").val(d);
    }else{
      var d=$("#paymentid").val();
    }
    $("a[name='paymentlink']").each(function(){
      if($(this).attr("id")=='paymentlink'+d){
        $(this).attr("class","current");
      }
    });
  },
  rebindcurrentpayment();
  rebindcurrentinvoice=function(){
    if(window.localStorage){
      var d=localStorage.getItem("invoiceid");
      $("#invoiceid").val(d);
    }else{
      var d=$("#invoiceid").val();
    }
    $("a[name='invoicelink']").each(function(){
      if($(this).attr("id")=='invoicelink'+d){
        $(this).attr("class","current");
      }
    });
  },
  rebindcurrentinvoice();

  saveinvoice=function(basepath){
    var invpayee=$("#invpayee").val();
    if(invpayee==""){
      $("#emsg2").text("请输入发票抬头");
      return false;
    }
    if(invpayee.length>45){
      $("#emsg2").text("输入的发票抬头太长了");
      return false;
    }
    $.post(basepath+"/mcenter/saveInvoice.action",{
      "invpayee":invpayee
    },function(data){
      if(data.sucflag){
        var url="";
        var goodsstr=$.query.get("goodsstr");
        if(goodsstr==""){
          url=basepath+"/order/initOrder.action"
        }else{
          url=basepath+"/order/initOrder.action?goodsstr="+goodsstr;
        }
        window.location.href=url;
      }else{
        //登录
        window.location.href=basepath+"/gologin.action";
      }
    });
  },

  payorder=function(basepath){
    var deliveraddressid=$("#deliveraddressid").val();
    var paymentid=$("#paymentid").val();
    var cartgoodsid=$("#cartgoodsid").val();
    var freight=$("#freight").val();
    var isgoodsfromcart=$("#isgoodsfromcart").val();
    var ordertag="1";
    var invoiceid=$("#invoiceid").val();
    var logisticsid=$("#logisticsid").val();
    $.post(basepath+"/order/payOrder.action",{
      "deliveraddressid":deliveraddressid,
      "paymentid":paymentid,
      "cartgoodsid":cartgoodsid,
      "freight":freight,
      "isgoodsfromcart":isgoodsfromcart,
      "ordertag":ordertag,
      "invoiceid":invoiceid,
      "logisticsid":logisticsid
    },function(data){
        alert("订单已经提交");

    });
  }


});