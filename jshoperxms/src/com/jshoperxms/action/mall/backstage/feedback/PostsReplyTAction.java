package com.jshoperxms.action.mall.backstage.feedback;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.jshoperxms.action.mall.backstage.base.BaseTAction;
import com.jshoperxms.action.utils.BaseTools;
import com.jshoperxms.action.utils.enums.BaseEnums.DataUsingState;
import com.jshoperxms.entity.PostsReplyT;
import com.jshoperxms.entity.PostsT;
import com.jshoperxms.entity.UserT;
import com.jshoperxms.service.PostsReplyTService;
import com.jshoperxms.service.PostsTService;
import com.jshoperxms.service.UsertService;
import com.jshoperxms.service.impl.Serial;

@Namespace("/mall/feedback/postsreply")
@ParentPackage("jshoperxms")
@InterceptorRefs({ @InterceptorRef("mallstack") })
public class PostsReplyTAction extends BaseTAction {

	@Resource
	private PostsReplyTService postsReplyTService;

	@Resource
	private UsertService usertService;
	
	@Resource
	private PostsTService postsTService;
	
	/**
	 * 保存帖子回复
	 * @return
	 */
	@Action(value = "save", results = { @Result(name = "json", type = "json") })
	public String savePostsReplayT() {
		Criterion criterion = Restrictions.eq("userid", BaseTools.getAdminCreateId());
		UserT usert = this.usertService.findOneByCriteria(UserT.class, criterion);
		PostsReplyT prt = new PostsReplyT();
		prt.setId(this.getSerial().Serialid(Serial.POSTSREPLYT));
		prt.setPostsid(this.getPostsId());
		prt.setReplycontent(this.getReplycontent());
		prt.setLoginname(usert.getRealname());
		prt.setUserid(usert.getUserid());
		prt.setNick(usert.getRealname());
		prt.setHeadpath(usert.getHeadpath());
		prt.setSmallimageurl(this.getImageurl());
		prt.setBigimageurl(this.getImageurl());
		prt.setReplyid(this.getReplyid());
		prt.setStatus(DataUsingState.USING.getState());
		prt.setUpdatetime(BaseTools.getSystemTime());
		prt.setCreatetime(BaseTools.getSystemTime());
		prt.setVersiont(1);
		this.postsReplyTService.save(prt);
		// 帖子评论数+1
		PostsT postsT = this.postsTService.findByPK(PostsT.class, this.getPostsId());
		if (postsT != null) {
			postsT.setReplynum(postsT.getReplynum()+1);
			this.postsTService.update(postsT);
		}
		this.setSucflag(true);
		return JSON;
	}

	/**
	 * 根据帖子ID查询所有回复
	 * 
	 * @return
	 */
	@Action(value = "findAllByPage", results = { @Result(name = "json", type = "json") })
	public String findAllPostsReplyTByPostsId() {
		int lineSize = this.getLength();
		Criterion criterion = Restrictions.and(Restrictions.eq("postsid", this.getPostsId())).add(Restrictions.ne("status", DataUsingState.DEL.getState()));
		recordsFiltered = recordsTotal = this.postsReplyTService.count(PostsReplyT.class, criterion).intValue();
		if (recordsTotal % this.getLength() == 0) {
			totalPage = this.recordsTotal / this.getLength();
		} else {
			totalPage = this.recordsTotal / this.getLength() + 1;
		}
		List<PostsReplyT> list = this.postsReplyTService.findByCriteriaByPage(PostsReplyT.class, criterion, Order.desc("updatetime"), this.getCurrentPage(), lineSize);
		if (!list.isEmpty()) {
			beanlists = new ArrayList<PostsReplyT>();
			beanlists = list;
			this.setSucflag(true);
		}
		return JSON;
	}

	private String postsId;
	private String replycontent;
	private String imageurl;
	private String replyid;
	private PostsReplyT bean;
	private List<PostsReplyT> beanlists;
	private List<Map<String, Object>> data = new ArrayList<Map<String, Object>>();
	private int recordsTotal = 0;
	private int recordsFiltered = 0;
	private int draw;// 表格控件请求次数
	/**
	 * 请求第几页
	 */
	private int start;
	/**
	 * 请求几条
	 */
	private int length=10;
	private boolean sucflag;
	private int currentPage;
	private int totalPage;

	public PostsReplyTService getPostsReplyTService() {
		return postsReplyTService;
	}

	public void setPostsReplyTService(PostsReplyTService postsReplyTService) {
		this.postsReplyTService = postsReplyTService;
	}

	public PostsReplyT getBean() {
		return bean;
	}

	public void setBean(PostsReplyT bean) {
		this.bean = bean;
	}

	public List<PostsReplyT> getBeanlists() {
		return beanlists;
	}

	public void setBeanlists(List<PostsReplyT> beanlists) {
		this.beanlists = beanlists;
	}

	public List<Map<String, Object>> getData() {
		return data;
	}

	public void setData(List<Map<String, Object>> data) {
		this.data = data;
	}

	public int getRecordsTotal() {
		return recordsTotal;
	}

	public void setRecordsTotal(int recordsTotal) {
		this.recordsTotal = recordsTotal;
	}

	public int getRecordsFiltered() {
		return recordsFiltered;
	}

	public void setRecordsFiltered(int recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}

	public int getDraw() {
		return draw;
	}

	public void setDraw(int draw) {
		this.draw = draw;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public boolean isSucflag() {
		return sucflag;
	}

	public void setSucflag(boolean sucflag) {
		this.sucflag = sucflag;
	}

	public String getPostsId() {
		return postsId;
	}

	public void setPostsId(String postsId) {
		this.postsId = postsId;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	public String getReplycontent() {
		return replycontent;
	}

	public void setReplycontent(String replycontent) {
		this.replycontent = replycontent;
	}

	public String getImageurl() {
		return imageurl;
	}

	public void setImageurl(String imageurl) {
		this.imageurl = imageurl;
	}

	public String getReplyid() {
		return replyid;
	}

	public void setReplyid(String replyid) {
		this.replyid = replyid;
	}

}
