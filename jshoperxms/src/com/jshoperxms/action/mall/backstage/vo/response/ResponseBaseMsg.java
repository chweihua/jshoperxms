package com.jshoperxms.action.mall.backstage.vo.response;

import java.io.Serializable;
/**
 * 基础的响应类
* @ClassName: ResponseBaseMsg 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author jcchen
* @date 2015年10月22日 下午5:58:23 
*
 */
public class ResponseBaseMsg implements Serializable{
	private boolean sucflag;
	/**
	 * 提示消息
	 */
	private String msg;
	/**
	 * token 请每次在请求时带上该参数（x-session-token）
	 */
	private String token;
	/**
	 * session过期
	 */
	private boolean sessionTimeOut;
	
	/**
	 * @return the sessionTimeOut
	 */
	public boolean isSessionTimeOut() {
		return sessionTimeOut;
	}
	/**
	 * @param sessionTimeOut the sessionTimeOut to set
	 */
	public void setSessionTimeOut(boolean sessionTimeOut) {
		this.sessionTimeOut = sessionTimeOut;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public boolean isSucflag() {
		return sucflag;
	}
	public void setSucflag(boolean sucflag) {
		this.sucflag = sucflag;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	
}
