package com.jshoperxms.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.jshoperxms.dao.DeliverAddressTDao;
import com.jshoperxms.entity.DeliverAddressT;


@Repository("deliverAddressTDao")
public class DeliverAddressTDaoImpl extends BaseTDaoImpl<DeliverAddressT> implements DeliverAddressTDao {

	private static final Logger log = LoggerFactory.getLogger(DeliverAddressTDaoImpl.class);


}