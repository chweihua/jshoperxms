package com.jshoperxms.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.jshoperxms.dao.SiteNavigationTDao;
import com.jshoperxms.entity.SiteNavigationT;


@Repository("siteNavigationTDao")
public class SiteNavigationTDaoImpl extends BaseTDaoImpl<SiteNavigationT> implements SiteNavigationTDao {
	

	private static final Logger log = LoggerFactory.getLogger(SiteNavigationTDaoImpl.class);
	

}