package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.GoodsDetailRpT;
import com.jshoperxms.service.GoodsDetailRpTService;
@Service("goodsDetailRpTService")
@Scope("prototype")
public class GoodsDetailRpTServiceImpl extends BaseTServiceImpl<GoodsDetailRpT>implements GoodsDetailRpTService {


}
