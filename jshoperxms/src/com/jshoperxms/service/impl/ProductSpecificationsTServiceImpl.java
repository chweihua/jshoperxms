package com.jshoperxms.service.impl;


import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.ProductSpecificationsT;
import com.jshoperxms.service.ProductSpecificationsTService;

@Service("productSpecificationsTService")
@Scope("prototype")
public class ProductSpecificationsTServiceImpl extends BaseTServiceImpl<ProductSpecificationsT>implements ProductSpecificationsTService {

}
